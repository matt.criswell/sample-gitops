apiVersion: metal3.io/v1alpha1
metadata:
  labels:
    cluster.sigs.k8s.io/cluster-name: mos
    kaas.mirantis.com/provider: baremetal
    kaas.mirantis.com/region: region-one
  namespace: lab
  name: r620four
kind: BareMetalHostProfile
spec:
  devices:
    - device:
        #byName: /dev/sda
        minSizeGiB: 60
        wipe: true
      partitions:
        - name: bios_grub
          sizeGiB: 0.00390625
          partflags: ['bios_grub']
        - name: uefi
          partflags: ['esp']
          sizeGiB: 0.2
        - name: config-2
          sizeGiB: 0.0625
        - name: lvm_root_part
          sizeGiB: 0
    - device:
        #byName: /dev/sdb
        minSizeGiB: 30
        wipe: true
      partitions:
        - name: lvm_lvp_part
          sizeGiB: 0
    - device:
        #byName: /dev/sdc
        minSizeGiB: 30
        wipe: true
    - device:
        #byName: /dev/sdd
        minSizeGiB: 30
        wipe: true
  volumeGroups:
    - name: lvm_root
      devices:
        - partition: lvm_root_part
    - name: lvm_lvp
      devices:
        - partition: lvm_lvp_part
  logicalVolumes:
    - name: root
      vg: lvm_root
      sizeGiB: 0
    - name: lvp
      vg: lvm_lvp
      sizeGiB: 0
  fileSystems:
    - fileSystem: vfat
      partition: config-2
    - fileSystem: vfat
      partition: uefi
      mountPoint: /boot/efi
    - fileSystem: ext4
      logicalVolume: root
      mountPoint: /
    - fileSystem: ext4
      logicalVolume: lvp
      mountPoint: /mnt/local-volumes/
  preDeployScript: |
    #!/bin/bash -ex
    echo $(date) 'pre_deploy_script done' >> /root/pre_deploy_done
  postDeployScript: |
    #!/bin/bash -ex
    echo $(date) 'post_deploy_script done' >> /root/post_deploy_done
  grubConfig:
    defaultGrubOptions:
      - 'GRUB_DISABLE_RECOVERY="true"'
      - 'GRUB_PRELOAD_MODULES=lvm'
      - 'GRUB_TIMEOUT=20'
  kernelParameters:
    sysctl:
      kernel.panic: "900"
      kernel.dmesg_restrict: "1"
      kernel.core_uses_pid: "1"
      fs.file-max: "9223372036854775807"
      fs.aio-max-nr: "1048576"
      fs.inotify.max_user_instances: "4096"
      vm.max_map_count: "262144"
