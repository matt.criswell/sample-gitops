apiVersion: lcm.mirantis.com/v1alpha1
kind: HelmBundle
metadata:
  name: coredns
  namespace: osh-system
spec:
  repositories:
  - name: hub_stable
    url: https://charts.helm.sh/stable
  releases:
  - name: coredns
    chart: hub_stable/coredns
    version: 1.8.1
    namespace: coredns
    values:
      image:
        repository: mirantis.azurecr.io/openstack/extra/coredns
        tag: "1.6.9"
      isClusterService: false
      servers:
      - zones:
        - zone: .
          scheme: dns://
          use_tcp: false
        port: 53
        plugins:
        - name: cache
          parameters: 30
        - name: errors
        - name: health
        - name: ready
        - name: kubernetes
          parameters: cluster.local
        - name: loadbalance
          parameters: round_robin
        - name: prometheus
          parameters: 0.0.0.0:9153
        - name: forward
          parameters: . /etc/resolv.conf
        - name: file
          parameters: /etc/coredns/prannon.net.db prannon.net
      serviceType: LoadBalancer
      zoneFiles:
      - filename: prannon.net.db
        domain: prannon.net
        contents: |
          prannon.net.            IN      SOA     sns.dns.icann.org. noc.dns.icann.org. 2015082541 7200 3600 1209600 3600
          prannon.net.            IN      NS      b.iana-servers.net.
          prannon.net.            IN      NS      a.iana-servers.net.
          prannon.net.            IN      A       172.16.3.107
          *.prannon.net.           IN      A      172.16.3.107
